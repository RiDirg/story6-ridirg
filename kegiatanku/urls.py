from django.urls import path
from .views import *
urlpatterns = [
    path('', kegiatanku_list),
    path('formkegiatan/', kegiatanku_form),
    path('deletekeg/<str:id_kegiatan>/', delete_kegiatan),
    path('deleteorg/<str:id_org>/', delete_orang),
]